package com.main;

import java.util.Scanner;

import com.service.Employee;

public class EmployeeMain {

	public static void main(String[] args) {

		Employee employee = new Employee();
		employee.setEmpName("venkat");
		employee.setEmpNo(1234);
		employee.setEmpSalary(2345f);
		employee.setAge(23);

		System.out.println(employee.getEmpNo());
		System.out.println(employee.getEmpName());
		System.out.println(employee.getEmpSalary());
		System.out.println(employee.getAge());

		System.out.println("Employee : " + employee.getEmpName());
		System.out.println("Employee : " + employee.getAge());
		System.out.println("Employee : " + employee.getEmpSalary());
		System.out.println("Employee : " + employee.getEmpName() + " " + employee.getEmpNo() + " " + employee.getAge() + " " + employee.getEmpSalary());

		Scanner scanner = new Scanner(System.in);
		int empAge;
		String empName;
		System.out.println("Enter Employee Name :");

		String name = scanner.nextLine();

		empName = employee.getEmpName();

		empAge = employee.getAge();
		System.out.println(name + " : " + empAge);

		/*
		 * Scanner scanner1 = new Scanner(System.in); int empNo = 1234; String empName =
		 * "venkat"; int empAge = 23; float empSalary = 2345f;
		 * System.out.println("Enter Employee No :");
		 * 
		 * String No = scanner1.nextLine();
		 * 
		 * String n = String.valueOf(No);
		 * 
		 * No = employee.getEmpName() + " "+ employee.getAge() + " " +
		 * employee.getEmpSalary();
		 * 
		 * System.out.println(empNo + " : " + empName + " " + empAge + " " + empSalary);
		 */

	}

}
